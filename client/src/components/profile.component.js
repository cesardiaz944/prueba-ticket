import React, { Component } from "react";

import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';

import AuthService from "../services/auth.service";
import TicketService from "../services/ticket.service";



const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
});



export default class Profile extends Component {

  
  constructor(props) {
    super(props);
    this.onChangeTitle = this.onChangeTitle.bind(this);
    
   

    this.state = {
      currentUser: null,
      classes : useStyles,
      rows : [],
      open: false,
      setOpen: false,
      openTitle: false,
      title: '',
      ticketSelect : ''
    };
    
  }

  createData(id, title, status , userId , role) {
    return { id, title, status, userId, role };
  }

  onChangeTitle(e) {
    this.title =  e.target.value
  }

  handleClickOpen = () => {
    this.setState({ open: true })
  };

  handleClose = () => {
    this.setState({ open: false })
  };

  handleClickOpenTitle = (row ) => {

   if(AuthService.getCurrentUser().role === "admin"){

    this.ticketSelect = row
    this.setState({ openTitle: true })
   }
      
    
  
  };

  handleCloseTitle = () => {
    this.setState({ openTitle: false })
  };

  
  acceptTicket = () => {

    TicketService.save().then(
      data => {
       
        window.location.reload();
      },
      error => {
      }
    );
    this.setState({ open: false })
  };

  acceptTicketTitle = () => {

    TicketService.update(this.ticketSelect.id, this.title).then(
      data => {
        window.location.reload();
      },
      error => {
      }
    );


    this.setState({ openTitle: false })
  };


  componentWillMount() {
    var tickets = []
    this.setState ({currentUser: AuthService.getCurrentUser()})
    TicketService.getAll().then(
      data => {
        data.forEach(element =>{
          tickets.push( this.createData( element.id, element.title, element.status, element.user.firstName, element.user.role)  )
        }
           
        );
        
        this.setState({ rows: tickets })
        
      },
      error => {
      }
    );
  }

  render() {
    const { currentUser , classes, rows, open, openTitle, ticketSelect} = this.state;
    return (
      <div className="container">
        <Button variant="outlined" class="float-right btn btn-primary m-3" color="primary" onClick={this.handleClickOpen}>
          Pedir Ticket
        </Button>

      <Dialog
        open={open}
        onClose={this.handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"¿Esta seguro de esta acción?"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Si acepta estará solicitando un ticket al administrador del sistema
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={this.handleClose} color="primary">
            Cancelar
          </Button>
          <Button onClick={this.acceptTicket} color="primary" autoFocus>
            Aceptar
          </Button>
        </DialogActions>
      </Dialog>

      <Dialog open={openTitle} onClose={this.handleCloseTitle} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Titulo</DialogTitle>
        <DialogContent>
          <DialogContentText>
           Por favor escriba el titulo del ticket
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="titulo"
            label="Titulo"
            onChange={this.onChangeTitle}
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={this.handleCloseTitle} color="primary">
            Cancelar
          </Button>
          <Button onClick={this.acceptTicketTitle} color="primary">
            Guardar
          </Button>
        </DialogActions>
      </Dialog>


         <TableContainer component={Paper}>
          <Table className={classes.table} aria-label="customized table">
            <TableHead>
              <TableRow>
                <StyledTableCell>Titulo</StyledTableCell>
                <StyledTableCell align="right">Estatus</StyledTableCell>
                <StyledTableCell align="right">Usuario</StyledTableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row) => (
                <StyledTableRow key={row.id}>
                  <StyledTableCell component="th" scope="row">
                  <Button  variant="outlined" color="primary"  onClick={ () => this.handleClickOpenTitle(row)}>
                      { row.title ? row.title : 'Sin Asignar'}
                  </Button>
                  </StyledTableCell>
                  <StyledTableCell align="right">{ row.status }</StyledTableCell>
                  <StyledTableCell align="right">{row.userId}</StyledTableCell>
                </StyledTableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    );
  }
}
