import axios from "axios";
import AuthService from './auth.service';

const API_URL = "http://localhost:3000/";

class TikectService {
  save( ) {
    var user = AuthService.getCurrentUser()
    return axios
      .post(API_URL+'tickets',{
        "title": "",
        "status": "Pedido",
        "userId": user.id
      })
      .then(response => {
        return response.data;
      });
  }

  update(id, title) {

    return axios.patch(API_URL+'tickets/'+id,{
        "title": title,
        "status": "Asignado"
      })
      .then(response => {
  
        return response.data;
      });
  }

  getAll() {
    var user = AuthService.getCurrentUser()
    var filter = {
      "include":[{"relation": "user"
     
        }
      ]
    } 
    if(user.role === "user"){
      return axios
      .get(API_URL+'/ticketByUser/'+user.id,filter)
      .then(response => {
        return response.data;
      });
    }
    return axios
      .get(API_URL+'/tickets', filter )
      .then(response => {
        return response.data;
      });
  }

 
}

export default new TikectService();
