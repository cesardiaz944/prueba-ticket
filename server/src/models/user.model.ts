
import {Entity, model, property, hasMany} from '@loopback/repository';
import {Ticket, TicketWithRelations} from './ticket.model';

@model({
  settings: {
    indexes: {
      uniqueEmail: {
        keys: {
          email: 1,
        },
        options: {
          unique: true,
        },
      },
    },
  },
})
export class User extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
  })
  id: string;

  @property({
    type: 'string',
    required: true,
  })
  email: string;

  @property({
    type: 'string',
    required: true,
  })
  firstName: string;

  @property({
    type: 'string',
    required: true,
  })
  password: string;

  @property({
    type: 'string',
    required: false,
  })
  role: string;

  @hasMany(() => Ticket)
  tickets: Ticket[];

  constructor(data?: Partial<User>) {
    super(data);
  }
}

export interface UserRelations {
  tickets?: TicketWithRelations[];
}

export type UserWithRelations = User & UserRelations;
