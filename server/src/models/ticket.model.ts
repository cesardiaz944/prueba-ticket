import {Entity, model, property, belongsTo} from '@loopback/repository';
import {User, UserWithRelations} from './user.model';

@model()
export class Ticket extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
  })
  id?: string;

  @property({
    type: 'string',
    required: false,
  })
  title: string;

  @property({
    type: 'string',
  })
  status?: string;



  @belongsTo(() => User)
  userId: string;

  getId() {
    return this.id;
  }

  constructor(data?: Partial<Ticket>) {
    super(data);
  }
}

export interface TicketRelations {
   user?: UserWithRelations;
}

export type TicketWithRelations = Ticket & TicketRelations;
